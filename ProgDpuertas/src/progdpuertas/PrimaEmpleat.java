
public class PrimaEmpleat {

    private String nom;
    private String directiu;
    private int antiguitat;
    private static final int PRIMA_DIRECTIU_S_MES_12 = 600;
    private static final int PRIMA_DIRECTIU_S_MENYS_IGUAL_12 = 400;
    private static final int PRIMA_DIRECTIU_N_MES_12 = 150;
    private static final int PRIMA_DIRECTIU_N_MENYS_IGUAL_12 = 100;
    private static final String MISSATGE_ERROR_CARGO = "El codi del càrrec ha de ser 'S' o 'N'";
    private static final String MISSATGE_ERROR_ANTIGUITAT = "La antiguitat ha de ser un nombre entre 0 i 999";

    public static void main(String[] args) {

        PrimaEmpleat e = new PrimaEmpleat("Elvira", "S", 11);
        int p = calcularPrima(e.directiu, e);
        if (p != 0) {
            mostrarPrima(e, p);
        }
    }

    public PrimaEmpleat(String nome, String dire, int ante) {
        this.nom = nome;
        this.directiu = dire;
        this.antiguitat = ante;
    }
    
    public static int calcularPrima(String directiu, PrimaEmpleat e) {
        int p = 0;
        if(e.antiguitat > 0 && e.antiguitat < 999){
            switch (directiu) {
            case "S":
                if (e.antiguitat > 12) {
                    p = PRIMA_DIRECTIU_S_MES_12;
                } else {
                    p = PRIMA_DIRECTIU_S_MENYS_IGUAL_12;
                }
                break;
            case "N":
                if (e.antiguitat > 12) {
                    p = PRIMA_DIRECTIU_N_MES_12;
                } else {
                    p = PRIMA_DIRECTIU_N_MENYS_IGUAL_12;
                }
                break;
            default:
                System.out.println("El codi del càrrec ha de ser 'S' o 'N'");
                break;
        }}
        else{
            p = 0;
            System.out.println(MISSATGE_ERROR_ANTIGUITAT);
        }
        return p;
    }

    private static void mostrarPrima(PrimaEmpleat e, int p) {
        System.out.println("La prima que li correspon a " + e.nom + " és de " + p + " Euros");
    }
}
