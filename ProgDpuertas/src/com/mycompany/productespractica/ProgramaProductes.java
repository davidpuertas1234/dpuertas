/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.productespractica;

import java.util.Scanner;

/**
 *
 * @author dpuertas.daw1r22
 */
public class ProgramaProductes {
	public static void main(String[] args) {
   	 
    	GestioFitxer g= new GestioFitxer();
    	Producte productes[] = new  Producte[50];
    	int quants=g.llegeix(productes);


       	// Aquí va el vostre codi
   	 
    	String producte[]={
        	"4LIFE TRANSFER FACTOR PLUS 90 TBS",
        	"AB SOLUTION PLUS 240ml",
        	"ABRASONE RECTAL crema 30 gr.",
        	"ABRAXANE 100mg 1vial 50ml",
        	"ACID HYALURONIC 20MG 30CAPS",
        	"ACID HYALURONIC 60MG 30CAPS TRIPLE STRENGTH",
        	"ACIDOPHILUS PLUS 60caps",
        	"ACOMPLIA 20mg 28 tabs",
        	"ACT eau dentaire fluor classic cinnamon fl 400 ml",
        	"ACTOVEGIN 10 ML 5 AMP",
        	"ACTOVEGIN FORTE 200 mg 100 dragees",
        	"ACTOVEGIN INJ. 5 X 5 ML",
        	"ADRENALIN 1:1000 ( 1mg/ ml ) 10x1 ml",
        	"AETHOXYSKLEROL 0.25% 5Ampo 2ml",
        	"AETHOXYSKLEROL 0.5% 5 Ampo 2ml",
        	"AETHOXYSKLEROL 1% 30 ml VIAL",
        	"AETHOXYSKLEROL 1% 5x2ml",
        	"AETHOXYSKLEROL 2% 5x2ml",
        	"AETHOXYSKLEROL 3% 5 Amp 2ml",
        	"AETHOXYSKLEROL 4% 5x2ml",
        	"AGGRENOX  200/25mg 60 caps",
        	"AGIOCUR Granulat 250g",
        	"AGIOLAX MITE 1000gr",
        	"AHAVA CREMA CORPORAL VAINILLA 300 GR.",
        	"AHAVA DERMUD BODY MILK 250 ml.",
        	"AHAVA DERMUD CR.HUMECTANT CALMANT 50 ML SPF15",
        	"AHAVA DERMUD CREMA COLZES I GENOLLS 75 ml.",
        	"AHAVA DERMUD CREMA CORPORAL 200 ML.",
        	"AHAVA DERMUD CREMA MANS 125 ML",
        	"AHAVA DERMUD CREMA NUTRITIVA 50 ML"};
	 
    	int vendesTrim [][]= {
    	{290,316,877,924},
    	{339,874,158,359},
    	{485,754,80,38},
    	{490,358,628,330},
    	{52,118,852,463},
    	{925,69,164,458},
    	{234,680,342,758},
    	{290,316,877,924},
    	{339,874,158,359},
    	{485,754,80,38},
    	{490,358,628,330},
    	{52,118,852,463},
    	{925,69,164,458},
    	{234,680,342,758},
    	{290,316,877,924},
    	{339,874,158,359},
    	{485,754,80,38},
    	{490,358,628,330},
    	{52,118,852,463},
    	{925,69,164,458},
    	{234,680,342,758},
    	{290,316,877,924},
    	{339,874,158,359},
    	{485,754,80,38},
    	{490,358,628,330},
    	{52,118,852,463},
    	{925,69,164,458},
    	{234,680,342,758},
    	{485,754,80,38},
    	{490,358,628,330}};
      	 
    	
    	
  	 
    	System.out.println("1. Carregar dades");
    	System.out.println("2. Lista de productes");
    	System.out.println("3. Nou producte");
    	System.out.println("4. Eliminar producte");
    	System.out.println("5. Màximes vendes per producte");
    	System.out.println("6. Mínimes vendes per producte");
        System.out.println("7. Mitjana de cada producte amb més vendes");
        System.out.println("8. Mitjana de cada producte amb menys vendes");
        System.out.println("9. AGRUPAR PRODUCTES pel nom i Mostrar el STOCK i l'emmagatzematge que ens queda (NULL)");
        System.out.println("10. Calcular beneficis MÀXIMS d'un producte");
        System.out.println("11. Calcular beneficis MÍNIMS d'un producte");
        System.out.println("12. EXIT");
   	
        Scanner lector = new Scanner(System.in);
    	int valor = lector.nextInt();
   	 
    	//Alumne al = new Alumne();
  	 
    	switch(valor){
        	case 1:
           	 
            	for(int i = 0; i < producte.length; i++){
                	Producte p = new Producte();
                	p.nom = producte[i];
               	 
                	for(int j = 0; j < vendesTrim[0].length; j++){
                    	p.vendes[j] = vendesTrim[i][j];
                   	 
                	}
                	productes[quants] = p;
                	quants++;
            	}
            	break;
           	 
           	
           	 
        	case 2:
            	
                	for (int z = 0; z < quants; z++){
                	System.out.println("");
                	System.out.print((z + 1)+". ");
                	System.out.print(productes[z].nom+" ");
                	System.out.printf("%50.50s","LES VENDES SON: ");
                	for (int j = 0; j < productes[z].vendes.length; j++){
                    	System.out.print(productes[z].vendes[j]);
                    	System.out.print(", ");
                   	 
                	}
            	}
               	 
           	 
           	 
           	 
           	 
            	break;
       	 
        	case 3:
           	 
            	Producte p = new Producte();
            	System.out.println("Introdueix el nom del producte  ");
            	p.nom = lector.next();
                System.out.println("Introdueix descripció del producte (TOT JUNT)(obligatori): ");
                p.nom = p.nom + " "+ lector.next();
                System.out.println("Vols posar una descripció adicional? [s/n]: ");
                if(lector.next().equals("s")){
                    System.out.println("Afegeix-la: ");
                    p.nom = p.nom + " "+ lector.next();
                }
                
            	productes[quants] = p;
           	 
           	 
            	System.out.println("Introdueix la primera venda");
            	p.vendes[0] = lector.nextDouble();
           	 
            	System.out.println("Introdueix la segona venda");
            	p.vendes[1] = lector.nextDouble();
           	 
            	System.out.println("Introdueix la tercera venda");
            	p.vendes[2] = lector.nextDouble();
           	 
            	System.out.println("Introdueix la cuarta venda");
            	p.vendes[3] = lector.nextDouble();
            	quants++;
            	break;
           	 
           	 
       	 
        	case 4:
            	System.out.println("Indica el número de l'element que vols esborrar: ");
            	int numero = lector.nextInt();
            	if (numero <= quants){
               	 
                	for(int i = numero - 1; i < quants - 1; i++){
                   	 
                    	productes[i] = productes[i + 1];

            	}
            	quants--;
            	break;
           	 
            	}
            	else{
                    	System.out.println("Ese número no existe en la lista: "+ quants);
            	}
           	 
               	 
        	case 5:
                    for(int i = 0; i < quants; i++){
                        double grande = productes[i].vendes[0];
                        for(int j = 0; j < productes[i].vendes.length; j++){
                            if(productes[i].vendes[j] > grande){
                                grande = productes[i].vendes[j];
                            }
                        }
                        
                        System.out.println(productes[i].nom+ " la seva venda més gran és de: "+ grande);
                    }
                    break;
       	 
           	 
           	 
           	 
           	 
        	case 6:
                    for(int i = 0; i < quants; i++){
                        double pequenio = productes[i].vendes[0];
                        for(int j = 0; j < productes[i].vendes.length; j++){
                            if(productes[i].vendes[j] < pequenio){
                                pequenio =  productes[i].vendes[j];
                            }
                        }
                        
                        System.out.println(productes[i].nom+ " la seva venda més petita és de: "+ pequenio);
                    }
                    break;
                    
                
                case 7:
                    int sumador = 0;
                    for(int i = 0; i < quants; i++){
                        double grande = productes[i].vendes[0];
                        for(int j = 0; j < productes[i].vendes.length; j++){
                            if(productes[i].vendes[j] > grande){
                                grande = productes[i].vendes[j];
                            }
                        }
                        sumador += grande;
                        
                    }
                    System.out.println("La mitjana dels productes més grans és: "+ (sumador / quants));
                    break;
           	
                case 8:
                    sumador = 0;
                    for(int i = 0; i < quants; i++){
                        double pequenio = productes[i].vendes[0];
                        for(int j = 0; j < productes[i].vendes.length; j++){
                            if(productes[i].vendes[j] < pequenio){
                                pequenio = productes[i].vendes[j];
                            }
                        }
                        sumador += pequenio;
                        
                    }
                    System.out.println("La mitjana dels productes més petits és: "+ (sumador / quants));
                    break;
                    
                case 9:
                    String palabra = "";
                    int contador = 0;  
                    int contador2 = 0;
                    int posicion = 0;
                    String lista_productos[] = new String[quants];
                    String grupos_productos[] = new String[50];
                    for(int i = 0; i < quants; i++){
                        int n = 0;
                        while (productes[i].nom.charAt(n) != ' '){
                            palabra += productes[i].nom.charAt(n);
                            n++;
                            
                        }
                        contador += 1;
                        if(contador == 1){
                            grupos_productos[0] = palabra;
                        }
                        lista_productos[i] = palabra;
                        
                        palabra = "";
                    }
                    for(int h = 0; h < lista_productos.length; h++){
                        for(int n = 0; n < grupos_productos.length; n++){
                            if(lista_productos[h].equals(grupos_productos[n])){
                                contador += 1;
                                
                            }
                            
                        }
                        
                        if(contador == 0){
                            grupos_productos[posicion + 1] = lista_productos[h];
                            posicion += 1;
                            contador2 += 1;
                            
                        }
                        contador = 0;
                        
                    }
                    for(int a = 0; a < grupos_productos.length; a++){
                        
                        System.out.println(grupos_productos[a]);
                        
                    }
                    System.out.println("Productes en stock: "+(contador2 + 1));
                    System.out.println("Emmagatzematge sobrant: "+ (grupos_productos.length - (contador2 + 1)));
                    break;
                
                case 10:
                    
                    for (int z = 0; z < quants; z++){
                	System.out.println("");
                	System.out.print((z + 1)+". ");
                	System.out.print(productes[z].nom+" ");
                	System.out.printf("%50.30s","LES VENDES SON: ");
                	for (int j = 0; j < productes[z].vendes.length; j++){
                    	System.out.print(productes[z].vendes[j]);
                    	System.out.print(", ");
                   	 
                	}
            	}
                    System.out.println("");
                    System.out.println("Escull un dels productes per calcular els beneficis: ");
                    int num_prod = lector.nextInt();
                    System.out.println(productes[num_prod - 1].nom+": ");
                    for (int j = 0; j < productes[num_prod - 1].vendes.length; j++){
                    	System.out.print(productes[num_prod - 1].vendes[j]);
                    	System.out.print(", ");
                	}
                    System.out.println("");
                    System.out.println("Cost del producte (posa ',' si el cost no és enter): ");
                    double cost_prod = lector.nextDouble();
                    
                    

                    double grande = productes[num_prod - 1].vendes[0];
                    for(int j = 0; j < productes[num_prod - 1].vendes.length; j++){
                        if(productes[num_prod - 1].vendes[j] > grande){
                            grande = productes[num_prod - 1].vendes[j];
                        }
                    }
                        
                    System.out.println(productes[num_prod - 1].nom+ " els seus beneficis serien de: "+ (grande * cost_prod));
                    break;
                    
                    
                case 11:    
                     for (int z = 0; z < quants; z++){
                	System.out.println("");
                	System.out.print((z + 1)+". ");
                	System.out.print(productes[z].nom+" ");
                	System.out.printf("%50.30s","LES VENDES SON: ");
                	for (int j = 0; j < productes[z].vendes.length; j++){
                    	System.out.print(productes[z].vendes[j]);
                    	System.out.print(", ");
                   	 
                	}
            	}
                    System.out.println("");
                    System.out.println("Escull un dels productes per calcular els beneficis: ");
                    int num_prod2 = lector.nextInt();
                    System.out.println(productes[num_prod2 - 1].nom+": ");
                    for (int j = 0; j < productes[num_prod2 - 1].vendes.length; j++){
                    	System.out.print(productes[num_prod2 - 1].vendes[j]);
                    	System.out.print(", ");
                	}
                    System.out.println("");
                    System.out.println("Cost del producte (posa ',' si el cost no és enter): ");
                    double cost_prod2 = lector.nextDouble();
                    

                    double pequenio = productes[num_prod2 - 1].vendes[0];
                        for(int j = 0; j < productes[num_prod2 - 1].vendes.length; j++){
                            if(productes[num_prod2 - 1].vendes[j] < pequenio){
                                pequenio =  productes[num_prod2 - 1].vendes[j];
                            }
                        }
                        
                        System.out.println(productes[num_prod2 - 1].nom+ " la seva venda més petita és de: "+ (pequenio * cost_prod2));
                    
                
                case 12:
                    break;
                    
    	}
      	 
      	 
      	 
      	 
      	 
    	g.escriu(productes, quants);   
      	 
	}
   	 
   	 
   	 
}
